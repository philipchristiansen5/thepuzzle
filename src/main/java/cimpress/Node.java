package cimpress;

import java.util.ArrayList;
import java.util.List;

import cimpress.helper.AreaPotential;
import cimpress.helper.Utils;
import cimpress.helper.XY;
import cimpress.json.JsonNode;
import cimpress.json.Square;



public class Node implements Comparable<Node>{
	private short areaRemaining;
	private boolean[][] puzzle;
	private short numberOfSquares;
	private Square recentAddedSquare;
	private byte width;
	private byte height;
	
	public Node(JsonNode initialPuzzle){		
		this.areaRemaining=Utils.countTrueInMatrix(initialPuzzle.getPuzzle());
		this.puzzle=initialPuzzle.getPuzzle();
		this.width= (byte) puzzle[0].length;
		this.height=(byte) puzzle.length;		
		this.numberOfSquares=0;
		this.recentAddedSquare=null;
	}
	public Node(Node parentNode) {
		this.areaRemaining=parentNode.getAreaRemaining();
		this.puzzle=parentNode.getPuzzle();			
		this.width=parentNode.getWidth();
		this.height=parentNode.getHeight();
		this.numberOfSquares=parentNode.getNumberOfSquares();
		this.recentAddedSquare=null;
	}
	
	/**
	 * The next relevant point is the next point to put a square.
	 * This is the next point in the puzzle which is true. The puzzle is read
	 * like a book from the top left corner.
	 * @return
	 */
	public XY getNextRelevantPoint(){
		//The first relevant point when no squares have been added. This only happens once 
		if(numberOfSquares==0){						
			return getFirstRelevantPoint();
		}
		
		//The next point is likely to be the one after the recent added square
		byte xPoint=(byte) (recentAddedSquare.getX()+recentAddedSquare.getSize());				
		byte yPoint=recentAddedSquare.getY();
		boolean isPointInsidePuzzleWidth=xPoint<getWidth();
		if(isPointInsidePuzzleWidth && (puzzle[yPoint][xPoint])){			
			return new XY(xPoint,yPoint);
		} else{
			return getRelevantPointWhenTheNextRelevantPointIsNotAfterRecentAddedSquare(xPoint,yPoint);			
		}
	}
	
	/**
	 * A square is added to the node.  
	 * @param newSquare
	 */
	public void addSquare(Square newSquare){
		byte squareSize=newSquare.getSize();
		this.puzzle=getCopyOfPuzzleWithAnAddedSquare(newSquare);
		this.numberOfSquares++;
		this.recentAddedSquare=newSquare;			
		this.areaRemaining=(short) (this.areaRemaining-squareSize*squareSize);
	}
	
	/**
	 * The method return an AreaPotential instance. The object have two fields:
	 * - maxSize. The largest possible square with origin in the XY coordinate
	 * - isMaxSizeTheBestSolution. This field is true, if the max size with certainty is a better solution than the smaller squares.
	 * 
	 * For instance, see the puzzle below.
	 * 0 0 1 1
	 * 0 0 1 1
	 * 1 1 1 1
	 * 
	 * @param xy
	 * @return
	 */
	public AreaPotential getAreaPotentialFromPoint(XY xy){		
		byte size=getMaxAreaPotentialSize(xy);
		if(size==1){ //When the maximum potential size=1, then there is no other potential sizes.
			return new AreaPotential(size,true);//byte[]{size,1};
		}
		
		//If three sides of a square are blocked then it's the optimal area size.  		
		boolean isMaxAreaTheOptimalSolutionFound=false;
		if(checkIfLeftSideOfSquareIsBlocked(xy, size)){
			//If left is true. Only one of the other sides needs to be true. 
			isMaxAreaTheOptimalSolutionFound = checkIfBottomSideIsBlocked(xy, size) || checkIfRightSideOfSquareIsBlocked(xy, size);
		} else{
			//If left is false. Both right and bottom needs to be true
			isMaxAreaTheOptimalSolutionFound = checkIfBottomSideIsBlocked(xy, size) && checkIfRightSideOfSquareIsBlocked(xy, size);
		}
		return isMaxAreaTheOptimalSolutionFound ? new AreaPotential(size,true) : new AreaPotential(size,false);
		
	}
	/**
	 * A shallow copy is made because it saves memory and a shallow copy 
	 * doesn't require a full matrix iteration.  
	 * A deep copy is made of the rows where the square is added.  
	 * @param newSquare
	 * @return
	 */
	private boolean[][] getCopyOfPuzzleWithAnAddedSquare(Square newSquare) {
		byte squareSize=newSquare.getSize();
		byte squareW=newSquare.getX();
		byte squareH=newSquare.getY();
		//A shallow copy is made of the rows.
		boolean[][] shallowAndDeepCopy=puzzle.clone();
		
		//A deep copy is made for rows where the values change
		for(int h=squareH;h<squareH+squareSize;h++){
			shallowAndDeepCopy[h]=puzzle[h].clone();
			for(int w=squareW;w<squareW+squareSize;w++){
				shallowAndDeepCopy[h][w]=false;
			}
		}
		return shallowAndDeepCopy;
	}	
	/**
	 * Each node have a reference to a square. This square will have 
	 * a reference to the previous square. By having this structure it's possible 
	 * to get all the previous squares, when the last square is added.
	 * These squares make up the solution.
	 * 
	 * @return list of all previous added squares.
	 */
	public List<Square> getListOfSquares(){		
		List<Square> list=new ArrayList<Square>();
		Square cSquare=this.getSquare();
		while(cSquare!=null){ //When cSquare is null the end is reached	
			list.add(cSquare);
			cSquare=cSquare.getSquareAddedBeforeThis();
		}
		return list;
	}
	/**
	 * Remaining true booleans in the puzzle.
	 * @return
	 */
	public short getAreaRemaining() {
		return areaRemaining;
	}
	
	
	private XY getRelevantPointWhenTheNextRelevantPointIsNotAfterRecentAddedSquare(byte xPoint,byte yPoint){
		for(byte y=yPoint;y<getHeight();y++){
			while(xPoint<getWidth()){
				if(puzzle[y][xPoint]){
					return new XY(xPoint,y);
				}
				xPoint++;
			}
			xPoint=0;
			puzzle[y]=null;
		}
		return null;
	}
	private XY getFirstRelevantPoint(){
		for(byte y=0;y<getHeight();y++){
			for(byte x=0;x<getWidth();x++){
				if(puzzle[y][x]){
					return new XY(x,y);
				}					
			}				
		}
		return null;
	}
	public boolean[][] getPuzzle() {
		return puzzle;
	}	
	/**
	 * The currently number of added squares for the node
	 * @return
	 */
	public short getNumberOfSquares(){
		return numberOfSquares;
	}
	/**
	 * @return puzzle width
	 */
	public byte getWidth(){
		return this.width;
	}
	/**
	 * @return puzzle height
	 */
	public byte getHeight(){
		return this.height;
	}
	
	
	private boolean checkIfBottomSideIsBlocked(XY xy, byte size) {
		//The bottom is blocked when the bottom of the puzzle is reached
		return (xy.getY()+size)==getHeight();
	}
	private boolean checkIfLeftSideOfSquareIsBlocked(XY xy,byte size) {		
		if(xy.getX()==0){
			//Left side is blocked if square is with x=0.
			return true;
		}
		
		//Check if last square is blocking left side
		if(numberOfSquares!=0){
			if(recentAddedSquare.getSize()>=size && recentAddedSquare.getY()==xy.getY()){			
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfRightSideOfSquareIsBlocked(XY xy,byte area) {
		//In case the right size of the puzzle is hit
		byte wStart=xy.getX();
		byte hStart=xy.getY();
		if(wStart+area==getWidth()){
			//The right side is blocked when the square touches the puzzle side
			return true;
		}
		
		if(hStart+area<getHeight()){ 
			//If the corner is not blocked, then the square size is not necessarily optimal 
			boolean isRightLowerCornerBlocked=!puzzle[hStart+area][wStart+area] || !puzzle[hStart+area][wStart+area-1] || !puzzle[hStart+area-1][wStart+area];
			if(!isRightLowerCornerBlocked){
				return false;
			}
		}
		
		for(int i=0;i<area-1;i++){
			//If the side have a true then it's not a perfect solution
			if(puzzle[i+hStart][wStart+area] && puzzle[i+hStart+1][wStart+area]){
				return false;
			}
		}
		return true;
	}
	/**
	 * This method gets the maximum square size possible in the position xy
	 * @param xy
	 * @return
	 */
	private byte getMaxAreaPotentialSize(XY xy){
		int maxMoveWidth= getWidth()-xy.getX();
		int maxMoveHeight=getHeight()-xy.getY();
		short maxWH=(short) Math.min(maxMoveWidth,maxMoveHeight);
		byte size=0;
		for(int i=0;i<maxWH;i++){
			int hCurrent=xy.getY()+i;
			int wCurrent=xy.getX()+i;
			if(puzzle[hCurrent][wCurrent]){
				for(short j=1;j<size+1;j++){
					boolean leftDirection=puzzle[hCurrent][wCurrent-j];
					boolean upDirection=puzzle[hCurrent-j][wCurrent];
					boolean isConditionOk=leftDirection && upDirection;
					if(!isConditionOk){
						return size;
					}
				}
				size++;
			} else{
				return size;
			}
		}
		return size;
	}	

	public Square getSquare(){
		return this.recentAddedSquare;
	}
	
	@Override
	public int compareTo(Node o) {
		return (int) ((o.getAreaRemaining()-this.getAreaRemaining()));
	}
	public String toString(){
		return "Node[No of Squares:"+this.getNumberOfSquares()+", Area Remaining: "+this.areaRemaining+"\n"+Utils.toStringSolution(this);
	}
}
