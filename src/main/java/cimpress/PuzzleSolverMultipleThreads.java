package cimpress;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import cimpress.json.JsonNode;
import cimpress.json.JsonSolution;
import cimpress.searchtree.SearchTreeCreator;
import cimpress.searchtree.SearchTreeCreatorToCertainLevel;

/**
 * I have made two PuzzleSolvers:
 * 1.PuzzleSolverSimple. Used in the first round of the tech challenge.
 * 2.PuzzleSolverMultipleThreads. Used in the final
 * 
 * PuzzleSolverSimple is solving the puzzle without using concurrency - This solver is easier to understand so you should start by understanding this.
 * PuzzleSolverMultipleThreads uses several threads to solve puzzle. This solver is faster, but also harder to understand.
 * @author Phil
 */
public class PuzzleSolverMultipleThreads {
	public final static String KEY="5a97c984dc83479fa4d05b8b10c61839";	
	private static int numberOfThreads;
	private static int poolSize;
	private static int searchTreeWidthLimit;
	private static ExecutorService executor;
	
	
	public static void main(String[] args) throws Exception{		
		//The settings below are the optimal concurrency settings for my laptop.
		//The settings might look a bit weird but is a result of 4 cores + 4 virtual cores.
		//To get number of cores use: Runtime.getRuntime().availableProcessors() 
		numberOfThreads=7;
		poolSize=3;
		executor = Executors.newFixedThreadPool(poolSize);
		
		//The maximal width of the search tree is defined below
		searchTreeWidthLimit=220000;		
		
		// I use the Spring RestTemplate to connect to the cimpress API.
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();			
		String jsonPuzzle=restTemplate.getForObject("http://techchallenge.cimpress.com/"+KEY+"/trial/puzzle", String.class);			
		
		//The JSON is parsed to a java object
		//The jsonNode contains the puzzle.
		JsonNode jsonNode=mapper.readValue(jsonPuzzle,JsonNode.class);
		
		//I want to divide the workload in several threads
		//The first thing I do is to find several of nodes, which all have the same number of added squares 
		List<Node> independentNodes = getSeveralNodes(jsonNode,searchTreeWidthLimit);
		
		//The nodes which all have the same number of squares added are divided into
		//several lists - one list for each thread.
		List<LinkedList<Node>> nodesDividedInLists = divideNodesIntoSeveralListsOneForEachThread(numberOfThreads, independentNodes);
		
		//The Threads are created and started.
		List<Future<Node>> futures = createThreadForEachListOfNodes(nodesDividedInLists);			
		
		//The best performing thread will return the Node with the "best" solution.
		Node node = getBestPerformingNodeOfTheThreads(futures);
		
		//The solution is submitted 
		String response=restTemplate.postForObject("http://techchallenge.cimpress.com/"+KEY+"/trial/solution",new JsonSolution(jsonNode.getId(),node.getListOfSquares()),String.class);		
		System.out.println(response);
		//The executor is closed down.
		executor.shutdown();
	}


	private static Node getBestPerformingNodeOfTheThreads(List<Future<Node>> futures) throws InterruptedException,ExecutionException {
		Node bestNode=null;
		for(Future<Node> future:futures){
			Node newNode=future.get();
			if(bestNode==null || newNode.getNumberOfSquares()<bestNode.getNumberOfSquares()){
				bestNode=newNode;
			}
		}
		return bestNode;
	}

	private static List<Future<Node>> createThreadForEachListOfNodes(List<LinkedList<Node>> linkedLists) {
		List<Future<Node>> futures=new ArrayList<>();
		for(LinkedList<Node> list:linkedLists){
			int numberOfLimitingQueuesPrThread=searchTreeWidthLimit/numberOfThreads;						
			futures.add(executor.submit(new SearchTreeCreator(list, numberOfLimitingQueuesPrThread)));
		}
		return futures;
	}

	private static List<LinkedList<Node>> divideNodesIntoSeveralListsOneForEachThread(int numberOfThreads, List<Node> smallList) {		
		List<LinkedList<Node>> linkedLists=new ArrayList<LinkedList<Node>>(numberOfThreads);		
		//First, I add some empty lists. 
		for(int i=0;i<numberOfThreads;i++){
			linkedLists.add(new LinkedList<Node>());
		}
		
		//The nodes are divided between lists. 
		int counter=0;
		for(Node node:smallList){		
			linkedLists.get(counter%numberOfThreads).add(node);			
			counter++;
		}
		return linkedLists;
	}
	
	private static List<Node> getSeveralNodes(JsonNode jsonRootNode,int searchTreeWidthLimit) throws Exception {
		SearchTreeCreatorToCertainLevel solver = new SearchTreeCreatorToCertainLevel(jsonRootNode, searchTreeWidthLimit);		
		List<Node> smallList=solver.call();
		System.out.println(smallList.size());
		return smallList;
	}
}
