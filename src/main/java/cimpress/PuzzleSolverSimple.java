package cimpress;

import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import cimpress.json.JsonNode;
import cimpress.json.JsonSolution;
import cimpress.searchtree.SearchTreeCreator;

/**
 * I have made two PuzzleSolvers:
 * 1.PuzzleSolverSimple. Used in the first round of the tech challenge.
 * 2.PuzzleSolverMultipleThreads. Used in the final
 * 
 * PuzzleSolverSimple is solving the puzzle without using concurrency - This solver is easier to understand so you should start by understanding this.
 * PuzzleSolverMultipleThreads uses several threads to solve puzzle. This solver is faster, but also harder to understand.
 * @author Phil
 */
public class PuzzleSolverSimple {
	public final static String KEY = "5a97c984dc83479fa4d05b8b10c61839";
	private static int searchTreeWidthLimit;

	public static void main(String[] args) throws Exception {
		// The maximal width of the search tree is defined below
		searchTreeWidthLimit = 110000;

		// I use the Spring RestTemplate to connect to the cimpress API.
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String jsonPuzzle = restTemplate.getForObject("http://techchallenge.cimpress.com/" + KEY + "/trial/puzzle",String.class);

		// The json is parsed to a Java instance of InitialPuzzle
		JsonNode initialPuzzle = mapper.readValue(jsonPuzzle,JsonNode.class);

		// The first node is created from the InitialPuzzle.
		Node node = new Node(initialPuzzle);

		// The solver finds a solution when the method call() is called.
		SearchTreeCreator solver = new SearchTreeCreator(node,searchTreeWidthLimit);
		node = solver.call();

		String response = restTemplate.postForObject("http://techchallenge.cimpress.com/" + KEY + "/trial/solution",new JsonSolution(initialPuzzle.getId(), node.getListOfSquares()),String.class);
		System.out.println(response);
	}
}
