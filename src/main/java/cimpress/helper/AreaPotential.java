package cimpress.helper;

public class AreaPotential {
	private byte maxSize;
	private boolean isMaxSizeTheBestSolution;

	public AreaPotential(byte maxSize,boolean isMaxSizeTheBestSolution) {
		this.maxSize = maxSize;
		this.isMaxSizeTheBestSolution = isMaxSizeTheBestSolution;				
	}
	public byte getMaxSize(){
		return this.maxSize;
	}
	/**
	 * The method returns a boolean which states if the maxSize is the optimal solution.
	 * 
	 * @return true means that only the maxSize square is created in new node. 
	 */
	public boolean isMaxSizeTheBestSolution(){
		return this.isMaxSizeTheBestSolution;
	}
}
