package cimpress.helper;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cimpress.Node;
import cimpress.json.JsonNode;


import cimpress.json.Square;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Utils {
	public static final String TRUE ="X ";
	public static final String FALSE="  ";
	public static final String WAS_TRUE=". ";
	
	public static short countTrueInMatrix(boolean[][] bools){
		int counter=0;
		for(int h=0;h<bools.length;h++){
			for(int w=0;w<bools[0].length;w++){
				if(bools[h][w]){
					counter++;
				}				
			}
		}
		return (short) counter;
	}
	
	public static String toStringSolution(Node node){
		List<Square> squaresList=node.getListOfSquares();		
		//byte[][] squares= new byte[3][squaresList.size()];
		//squares= squaresList.toArray(squares);
		String string="";		
		byte[][] solutionPuzzle=new byte[node.getHeight()][node.getWidth()];
		for(Square square:squaresList){
			//Square square=squares[i];
			byte wTemp=square.getX();
			byte hTemp=square.getY();
			byte size=square.getSize();
			for(int h=0;h<size;h++){
				for(int w=0;w<size;w++){
					solutionPuzzle[h+hTemp][w+wTemp]=size;
				}
			}
		}
		
		return toStringMatrixShort(solutionPuzzle);
	}
	public static String toStringMatrixShort(byte[][] puzzle){
		int height=puzzle.length;
		int width=puzzle[0].length;
		String puzzleRep="";
		for(int h=0;h<height;h++){
			for(int w=0;w<width;w++){
				short value=puzzle[h][w];
				if(value>9){
					puzzleRep+=value+" ";
				}else{
					puzzleRep+=value+"  ";
				}
				
				if(w==width-1){
					puzzleRep+="\n";
				}
			}			
		}			
		return puzzleRep;
	}
	
	public static String toStringMatrix(boolean[][] puzzle){
		int height=puzzle.length;
		int width=puzzle[0].length;
		String puzzleRep="";
		for(int h=0;h<height;h++){
			for(int w=0;w<width;w++){
				if(puzzle[h][w]){
					puzzleRep+=TRUE;
				} else{
					puzzleRep+=FALSE;
				}				
				if(w==width-1){
					puzzleRep+="\n";
				}
			}			
		}			
		return puzzleRep;
	}
	
	public static List<JsonNode> getInitialPuzzlesFromFiles() throws JsonParseException, JsonMappingException, IOException {		
		File dir = new File("jsonsTrial/");
		File[] files;
		
		files=dir.listFiles(new FilenameFilter(){
			public boolean accept(File dir, String name) {				
				return name.endsWith(".txt");				
			}			
		});
		ObjectMapper mapper=new ObjectMapper();
		List<JsonNode> list=new ArrayList<JsonNode>();
		for(int i=0;i<files.length;i++){			
			list.add(mapper.readValue(files[i],JsonNode.class));
		}	
		return list;
	}
}
