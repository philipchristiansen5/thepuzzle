package cimpress.helper;

public class XY {
	private byte x;
	private byte y;
	public XY(byte x,byte y) {
		this.x = x;
		this.y = y;				
	}
	public byte getX() {
		return x;
	}
	public byte getY() {
		return y;
	}	
}
