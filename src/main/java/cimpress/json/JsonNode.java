package cimpress.json;

import cimpress.helper.Utils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonNode {
	private String id;
	private int width;
	private int height;
	/** 	 
	 * (1,1) (1,2) (1,3) (1,4)
	 * (2,1) (2,2) (2,3) (2,4)
	 * (3,1) (3,2) (3,3) (3,4)
	 */	
	private boolean[][] puzzle;	
	
	public JsonNode() {}
	public JsonNode(boolean[][] puzzle){
		this();
		this.puzzle=puzzle;
		this.id="FAKE";
		this.width=puzzle[0].length;
		this.height=puzzle.length;
		
	}	
	
	public String getId() {
		return id;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public boolean[][] getPuzzle(){
		return puzzle;
	}
	
	public String toString(){
		return "id:"+id+", w:"+width+", h:"+height+"\n"+Utils.toStringMatrix(puzzle);
	}
	
	
	
}
