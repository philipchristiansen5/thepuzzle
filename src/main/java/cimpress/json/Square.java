package cimpress.json;

import cimpress.helper.XY;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Square {
	
	@JsonProperty("X")
	private byte x;
	@JsonProperty("Y")
	private byte y;
	@JsonProperty("Size")
	private byte size;
	
	@JsonIgnore
	private Square squareAddedBeforeThis;
		
	public Square(XY wh, byte size,Square squareAddedBeforeThis) {
		this.squareAddedBeforeThis = squareAddedBeforeThis;
		this.x=wh.getX();
		this.y=wh.getY();
		this.size=size;
	}
	
	public byte getX() {
		return x;
	}
	
	public byte getY() {
		return y;
	}
	public byte getSize() {
		return size;
	}	
	public String toString(){
		return "Square[x:"+x+", y:"+y+", size:"+size+"]";
	}
	public Square getSquareAddedBeforeThis(){
		return this.squareAddedBeforeThis;
	}
}
