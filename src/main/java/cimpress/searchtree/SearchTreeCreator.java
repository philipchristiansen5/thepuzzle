package cimpress.searchtree;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

import cimpress.Node;
import cimpress.helper.AreaPotential;
import cimpress.helper.XY;
import cimpress.json.Square;


public class SearchTreeCreator implements Callable<Node>{
	private LinkedList<Node> nodes;
	private int searchTreeWidthLimit;
	private int currentNumberOfSquares;
	
	/**
	 * Several nodes are supplied each with the same amount 
	 * @param subList
	 * @param searchTreeWidthLimit
	 */
	public SearchTreeCreator(List<Node> subList,int searchTreeWidthLimit) {
		this.nodes = (LinkedList<Node>) subList;
		this.searchTreeWidthLimit = searchTreeWidthLimit;
	}
	/**
	 * The rootNode of a puzzle is provided. The search tree becomes higher and higher for each
	 * step resulting in a wider and wider search tree. 
	 * When the width of the search tree exceeds the searchTreeWidthLimit, a worst nodes are filtered out   
	 * 
	 * @param rootNode
	 * @param searchTreeWidthLimit 
	 */
	public SearchTreeCreator(Node rootNode,int searchTreeWidthLimit) {		
		this.nodes = new LinkedList<Node>();
		this.nodes.add(rootNode);			
		this.searchTreeWidthLimit = searchTreeWidthLimit;
	}
	
	@Override
	public Node call() throws Exception {		
		currentNumberOfSquares = 0;
		short areaRemainingValuesAboveThisLimitIsFilteredOut = 0;
		Node node=null;
		while (!nodes.isEmpty()) {
			node = nodes.pollFirst();
			int currentSize = nodes.size();
			
			if (node.getAreaRemaining() == 0) {
				//Break if a puzzle is completed.
				//This breaks the while loop.
				break;
			}
			
			boolean isNewLevelInSearchTreeReached=currentNumberOfSquares < node.getNumberOfSquares();			
			if (isNewLevelInSearchTreeReached) {
				currentNumberOfSquares = node.getNumberOfSquares();
				boolean shouldWeReduceWidth=searchTreeWidthLimit <= currentSize;
				if (shouldWeReduceWidth) {					
					areaRemainingValuesAboveThisLimitIsFilteredOut = findLimitOfAreaRemaining();					
				}
			}
			
			if ((currentSize > searchTreeWidthLimit) && node.getAreaRemaining() > areaRemainingValuesAboveThisLimitIsFilteredOut) {
				// A node is discarded because the node performing poorly - In other words, the areaRemaining value is higher than the allowed limit
				//Furthermore, the search tree width is larger than the searchTreeWidthLimit 				
			} else {				
				addChildrenNodesToSearchTreeBasedOnParentNode(node);
			}
		}
		return node;
	}
	private void addChildrenNodesToSearchTreeBasedOnParentNode(Node node) {
		XY xy = node.getNextRelevantPoint();
		AreaPotential areaPotential = node.getAreaPotentialFromPoint(xy);				
		for (byte area = areaPotential.getMaxSize(); 0 < area; area--) {
			Node newNode = new Node(node);
			newNode.addSquare(new Square(xy,area,node.getSquare()));
			nodes.addLast(newNode);					
			if (areaPotential.isMaxSizeTheBestSolution()) { 
				break;
			}
		}
	}

	private short findLimitOfAreaRemaining() {
		short[] shorts=new short[nodes.size()];
		int counter=0;
		for(Node node:nodes){
			shorts[counter]=node.getAreaRemaining();
			counter++;
		}
		Arrays.sort(shorts);		
		return shorts[searchTreeWidthLimit-2];
	}
}
