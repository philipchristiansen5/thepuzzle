package cimpress.searchtree;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

import cimpress.Node;
import cimpress.helper.AreaPotential;
import cimpress.helper.XY;
import cimpress.json.JsonNode;
import cimpress.json.Square;

/**
 * This class returns a list of nodes which all have added the same number of squares.
 * @author Phil
 */
public class SearchTreeCreatorToCertainLevel implements Callable<List<Node>> {
	private LinkedList<Node> nodes;	
	private int limitSizeToReturnNodes;
	private int currentNumberOfSquares;

	public SearchTreeCreatorToCertainLevel(JsonNode jsonRootNode,int searchTreeWidthWhereNodesShouldBeReturned) {		
		this.limitSizeToReturnNodes = searchTreeWidthWhereNodesShouldBeReturned;
		this.nodes = new LinkedList<Node>();
		Node rootNode=new Node(jsonRootNode);
		nodes.addLast(rootNode);
	}
	/**
	 * Returns a list of nodes which all have the same number of 
	 */
	@Override
	public List<Node> call() throws Exception {		
		currentNumberOfSquares = 0;
		Node node=null;
		while (!nodes.isEmpty()) {
			node = nodes.pollFirst();
			int currentSize = nodes.size();			
			boolean isLowerLevelInSearchTreeReached=currentNumberOfSquares < node.getNumberOfSquares();			
			if (isLowerLevelInSearchTreeReached) {
				currentNumberOfSquares = node.getNumberOfSquares();
				boolean shouldNodesBeReturned=limitSizeToReturnNodes <= currentSize;
				if (shouldNodesBeReturned) {
					//The node is added to the list again before list is returned
					nodes.add(node); 
					return nodes;
				}
			}
			addChildrenNodesToSearchTreeBasedOnParentNode(node);			
		}
		//This code will never be reached.
		return null;
	}
	private void addChildrenNodesToSearchTreeBasedOnParentNode(Node node) {
		XY xy = node.getNextRelevantPoint();
		AreaPotential areaPotential = node.getAreaPotentialFromPoint(xy);
		for (byte area = areaPotential.getMaxSize(); 0 < area; area--) {
			Node newStep = new Node(node);
			newStep.addSquare(new Square(xy,area,node.getSquare()));
			nodes.addLast(newStep);
			if (areaPotential.isMaxSizeTheBestSolution()) {
				// This means that the best solution is found for 
				//the max area and hence no other new Nodes are created 				
				break;
			}
		}
	}
}
