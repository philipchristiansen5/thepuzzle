

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.PriorityQueue;

import org.junit.Before;
import org.junit.Test;

import cimpress.Node;
import cimpress.helper.AreaPotential;
import cimpress.helper.XY;
import cimpress.json.JsonNode;
import cimpress.json.Square;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class NodeTest {
	private boolean[][] puzzle;
	private JsonNode iPuzzle;
	private Node node;

	@Before
	public void setup(){
		//1 1 1 0
		//1 1 1 1
		//1 1 1 1
		//1 1 1 0
		puzzle=new boolean[][]{{true,true,true,false},{true,true,true,true},{true,true,true,true},{true,true,true,false}};
		iPuzzle=new JsonNode(puzzle);
		node = new Node(iPuzzle);
	}
	@Test
	public void testingSquareWithSizeTwo() {
		puzzle = new boolean[][] { 	{ false, false, false, false,false },
									{ true, true, true, true,true}, 
									{ true, true, true, true,false},
									{ true, true, true, true,true} };
		iPuzzle = new JsonNode(puzzle);
		node = new Node(iPuzzle);
		XY xy=new XY((byte)0,(byte)1);
		node.addSquare(new Square(xy,(byte) 2,null));
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(new XY((byte)2,(byte)1));
		assertTrue(areaPotential.isMaxSizeTheBestSolution());
	}
	@Test
	public void testingSquareWithSizeThreeWhenTheLastSquareWasSizeTwo() {
		puzzle = new boolean[][] { 	{ true, true, true, true,true,false},
									{ true, true, true, true,true,false}, 
									{ true, true, true, true,true,true},
									{ true, true, true, true,false,true} };
		iPuzzle = new JsonNode(puzzle);
		node = new Node(iPuzzle);
		XY xy=new XY((byte)0,(byte)0);
		node.addSquare(new Square(xy,(byte)2,null));
		xy=new XY((byte)2,(byte)0);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		assertFalse(areaPotential.isMaxSizeTheBestSolution());
		assertEquals(3,areaPotential.getMaxSize());
	}
	@Test
	public void testingSquareWithSizeThreeWhenTheLastSquareWasSizeThree() {
		puzzle = new boolean[][] { 	{ true, true, true, true,true,true,false},
									{ true, true, true, true,true,true,false}, 
									{ true, true, true, true,true,true,true},
									{ true, true, true, true,true,false,true} };
		iPuzzle = new JsonNode(puzzle);
		node = new Node(iPuzzle);
		XY xy=new XY((byte)0,(byte)0);
		node.addSquare(new Square(xy,(byte)3,null));
		xy=new XY((byte)3,(byte)0);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		
		assertEquals(3,areaPotential.getMaxSize());
		assertTrue(areaPotential.isMaxSizeTheBestSolution());
	}
	@Test
	public void theLeftSideIsAndTheRightSideIsBlocked() {
		puzzle = new boolean[][] { 	{ false, false, false, false },
									{ true, true, false, false}, 
									{ true, true, false, false},
									{ true, true, true, true } };
		iPuzzle = new JsonNode(puzzle);
		node = new Node(iPuzzle);		
		XY xy=new XY((byte)0,(byte)1);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		assertEquals(2, areaPotential.getMaxSize());
		assertTrue(areaPotential.isMaxSizeTheBestSolution());
	}
	
	@Test
	public void theLeftSideIsAndTheRightSideIsBlockedTwo() {
		puzzle = new boolean[][] { 	{ true, true, true, false},
									{ true, true, true, true}, 
									{ true, true, true, false},
									{ true, true, true, true } };
		iPuzzle = new JsonNode(puzzle);
		node = new Node(iPuzzle);
		XY xy=new XY((byte)0,(byte)0);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		assertEquals(3, areaPotential.getMaxSize());
		assertTrue(areaPotential.isMaxSizeTheBestSolution());		
	}
	@Test
	public void theLeftSideIsAndTheRightSideIsBlockedFour() {
		puzzle = new boolean[][] { 	{ false, false, false, false},
									{ true, true, true, true}, 
									{ true, true, true, false},
									{ true, true, true, true } };
		iPuzzle = new JsonNode(puzzle);
		node = new Node(iPuzzle);
		XY xy=new XY((byte)0,(byte)1);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		assertEquals(3, areaPotential.getMaxSize());
		assertTrue(areaPotential.isMaxSizeTheBestSolution());
		
	}
	@Test
	public void theLeftSideIsAndTheRightSideIsBlockedThree() {
		puzzle = new boolean[][] { 	{ true, true, true, false},
									{ true, true, true, true}, 
									{ true, true, true, true},
									{ true, true, true, false } };
		iPuzzle = new JsonNode(puzzle);
		node = new Node(iPuzzle);
		XY xy=new XY((byte)0,(byte)0);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		assertEquals(3, areaPotential.getMaxSize());
		assertFalse(areaPotential.isMaxSizeTheBestSolution());		
	}
	
	@Test
	public void theLeftSideIsAndTheRightSideIsBlockedFive() {
		puzzle = new boolean[][] { 	{ true, true, true, false},
									{ true, true, true, false}, 
									{ true, true, true, true},
									{ true, true, true, true } };
		iPuzzle = new JsonNode(puzzle);
		node = new Node(iPuzzle);
		XY xy=new XY((byte)0,(byte)0);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		assertEquals(3, areaPotential.getMaxSize());
		assertFalse(areaPotential.isMaxSizeTheBestSolution());
	}
	
	@Test
	public void testButtonAndRightSide(){
		puzzle = new boolean[][] { 	{ false, false, false, false},
				{ false, false, false, false}, 
				{ true, true, true, true},
				{ true, true, true, true } };
		iPuzzle = new JsonNode(puzzle);
		node = new Node(iPuzzle);		
		XY xy=new XY((byte)2,(byte)2);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		assertEquals(2, areaPotential.getMaxSize());
		assertTrue(areaPotential.isMaxSizeTheBestSolution());
	}
	
	@Test
	public void testAddSquare(){		
		Square square=new Square(new XY((byte)0,(byte)0),(byte)1,null);//w=0,h=0,size=1;
		node.addSquare(square);		
		boolean[][] deepAndShallow=node.getPuzzle();
		
		//The first row is a deep copy
		assertTrue(puzzle[0][0]!=deepAndShallow[0][0]);
				
		//The second row is a shallow copy so a change should happen in both
		puzzle[1][0]=false;
		assertTrue(puzzle[1][0]==deepAndShallow[1][0]);
	}
	@Test
	public void testAreaRemaining(){		
		assertEquals(14,node.getAreaRemaining());
		int prevArea=node.getAreaRemaining();				
		Square square=new Square(new XY((byte)0,(byte)0),(byte)2,null);
		node.addSquare(square);
		int afterArea=node.getAreaRemaining();		
		int expected=prevArea-square.getSize()*square.getSize();
		assertEquals(expected,afterArea);
	}	
	
	@Test
	public void testAreaPotentialFromPoint(){
		//1 1 1 0
		//1 1 1 1
		//1 1 1 1
		//1 1 1 0
		AreaPotential area=node.getAreaPotentialFromPoint(new XY((byte)0,(byte)0)); assertEquals(3,area.getMaxSize());
		
		area=node.getAreaPotentialFromPoint(new XY((byte)1,(byte)1)); assertEquals(2,area.getMaxSize()); assertFalse(area.isMaxSizeTheBestSolution());
		area=node.getAreaPotentialFromPoint(new XY((byte)2,(byte)1)); assertEquals(2,area.getMaxSize()); assertFalse(area.isMaxSizeTheBestSolution());
		area=node.getAreaPotentialFromPoint(new XY((byte)2,(byte)2)); assertEquals(1,area.getMaxSize()); assertTrue(area.isMaxSizeTheBestSolution());
		area=node.getAreaPotentialFromPoint(new XY((byte)3,(byte)0)); assertEquals(0,area.getMaxSize()); assertFalse(area.isMaxSizeTheBestSolution());
		area=node.getAreaPotentialFromPoint(new XY((byte)3,(byte)1)); assertEquals(1,area.getMaxSize()); assertTrue(area.isMaxSizeTheBestSolution());
		area=node.getAreaPotentialFromPoint(new XY((byte)3,(byte)2)); assertEquals(1,area.getMaxSize()); assertTrue(area.isMaxSizeTheBestSolution());
		area=node.getAreaPotentialFromPoint(new XY((byte)3,(byte)3)); assertEquals(0,area.getMaxSize()); assertFalse(area.isMaxSizeTheBestSolution());
		area=node.getAreaPotentialFromPoint(new XY((byte)2,(byte)0)); assertEquals(1,area.getMaxSize()); assertTrue(area.isMaxSizeTheBestSolution());
	}
	
	
	@Test
	public void testAreaPotentialIsBottomHit1(){
		puzzle=new boolean[][]{{false,false,false,false},{true,true,true,true},{true,true,true,true},{true,true,true,true}};
		iPuzzle=new JsonNode(puzzle);
		node = new Node(iPuzzle);
		//0 0 0 0  This line would be zeros if we get to 0,1
		//1 1 1 1
		//1 1 1 1
		//1 1 1 1
		
		XY xy=new XY((byte)0,(byte)1);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		assertEquals(3, areaPotential.getMaxSize());
		assertTrue(areaPotential.isMaxSizeTheBestSolution());
		
	}
	@Test
	public void testAreaHitWall(){		
		//0 0 0 0 
		//1 1 1 1
		//1 1 1 1
		//1 1 1 1
		puzzle=new boolean[][]{{false,false,false,false},{true,true,true,true},{true,true,true,true},{true,true,true,true}};
		iPuzzle=new JsonNode(puzzle);
		node = new Node(iPuzzle);
		
		XY xy=new XY((byte)0,(byte)1);
		node.addSquare(new Square(xy,(byte)2,null));
		xy=new XY((byte)2,(byte)1);
		AreaPotential areaPotential=node.getAreaPotentialFromPoint(xy);
		assertEquals(2, areaPotential.getMaxSize());
		assertTrue(areaPotential.isMaxSizeTheBestSolution());
	}
	@Test
	public void testSizeOfOneShouldAlwaysResultInTheBestSolution(){		
		//0 0 0 0
		//0 0 1 1
		//1 0 1 1
		//1 1 1 1
		puzzle=new boolean[][]{{false,false,false,false},{false,false,true,true},{true,false,true,true},{true,true,true,true}};
		iPuzzle=new JsonNode(puzzle);
		node = new Node(iPuzzle);		
		AreaPotential area=node.getAreaPotentialFromPoint(new XY((byte)0,(byte)2)); 
		assertEquals(1,area.getMaxSize());
		assertTrue(area.isMaxSizeTheBestSolution());
	}
	
	@Test
	public void testFirstRelevantPoint(){
		XY wh=node.getNextRelevantPoint();
		assertEquals(0,wh.getY());
		assertEquals(0,wh.getX());
	}
	@Test
	public void testFirstRelevantPointWhenXIsOne(){
		Node nodeOne=new Node(new JsonNode(new boolean[][]{{false,true},{true,true}}));
		XY wh=nodeOne.getNextRelevantPoint();
		assertEquals(1,wh.getX());
		assertEquals(0,wh.getY());
	}
	@Test
	public void testFirstRelevantPointWhenYIsOne(){
		Node nodeOne=new Node(new JsonNode(new boolean[][]{{false,false},{true,true}}));
		XY wh=nodeOne.getNextRelevantPoint();
		assertEquals(0,wh.getX());
		assertEquals(1,wh.getY());
	}
	
	@Test
	public void testOldSqauresInTwoNodess(){
		
		node.addSquare(new Square(new XY((byte)0,(byte)0),(byte)1,null));		
		
		Node nodeOne=new Node(node);
		Node nodeTwo=new Node(node);
		nodeOne.addSquare(new Square(new XY((byte)1,(byte)0),(byte)1,null));
		nodeTwo.addSquare(new Square(new XY((byte)1,(byte)1),(byte)2,null));
		boolean oneShouldBeFalse=nodeOne.getPuzzle()[0][1];
		boolean twoShouldBeTrue=nodeTwo.getPuzzle()[0][1];
		
		boolean oneShouldBeTrue=nodeOne.getPuzzle()[1][1];
		boolean twoShouldBeFalse=nodeTwo.getPuzzle()[1][1];
		
		assertTrue(oneShouldBeTrue);
		assertTrue(twoShouldBeTrue);
		assertFalse(oneShouldBeFalse);
		assertFalse(twoShouldBeFalse);
	}
	@Test
	public void testNextRelevantPointSimple(){
		node.addSquare(createNewSquare(0,0,1));
		XY wh=node.getNextRelevantPoint();
		assertEquals(0,wh.getY());
		assertEquals(1,wh.getX());
	}
	@Test
	public void testNextRelevantPointTheEnd(){
		node.addSquare(createNewSquare(2,3,1));
		XY wh=node.getNextRelevantPoint();
		assertNull(wh);
	}
	private Square createNewSquare(int x,int y,int size){
		XY xy =new XY((byte)x,(byte)y);
		return new Square(xy,(byte)size,null);
	}
	@Test
	public void testNextRelevantPointSqaureToWidth(){
		node.addSquare(createNewSquare(2,0,2));
		XY wh=node.getNextRelevantPoint();
		assertEquals(0,wh.getX());
		assertEquals(1,wh.getY());		
		node.addSquare(createNewSquare(wh.getX(),wh.getY(),2));
		wh=node.getNextRelevantPoint();
		assertEquals(2,wh.getY());
		assertEquals(2,wh.getX());
		node.addSquare(createNewSquare(wh.getX(),wh.getY(),1));
		wh=node.getNextRelevantPoint();
		assertEquals(2,wh.getY());
		assertEquals(3,wh.getX());
	}
}
